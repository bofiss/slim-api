<?php
require '../vendor/autoload.php';
require '../src/models/customer.php';
require '../src/handlers/exceptions.php';
require '../src/routes/routes.php';

$app->run();
