<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$config = include('../src/config.php');
$app = new \Slim\App(['settings' => $config]);
$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$capsule->getContainer()->singleton(
  Illuminate\Contracts\Debug\ExceptionHandler::class,
  App\Exceptions\Handler::class
);

// get all customer
$app->get('/api/customers', function (Request $request, Response $response) {
  return $response->getBody()->write(Customer::all()->toJson());
});

// retrieve a customer
$app->get('/api/customers/{id}', function (Request $request, Response $response, $args) {
  $id = $args['id'];
  return $response->getBody()->write(Customer::find($id)->toJson());
});

// add customer
$app->post('/api/customers', function (Request $request, Response $response, $args) {
  $data = $request->getParsedBody();
  $customer = new Customer();
  $customer->first_name = $data['first_name'];
  $customer->last_name = $data['last_name'];
  $customer->email= $data['email'];
  $customer->address = $data['address'];
  $customer->city = $data['city'];
  $customer->state = $data['state'];
  $customer->save();
  return $response->withStatus(201)->getBody()->write($customer->toJson());
});

// update customer data
$app->put('/api/customers/{id}', function (Request $request, Response $response, $args) {
  $id = $args['id'];
  $data = $request->getParsedBody();
  $customer = Customer::find($id);

  $customer->first_name = $data['first_name'] ?: $customer->first_name;
  $customer->last_name = $data['last_name'] ?: $customer->last_name;
  $customer->email= $data['email'] ? :   $customer->email;
  $customer->address = $data['address'] ? :$customer->address ;
  $customer->city = $data['city'] ?: $customer->city;
  $customer->state = $data['state'] ?: $customer->state;

  $customer->save();

  return $response->getBody()->write($customer->toJson());
});

// delete customer data
$app->delete('/api/customers/{id}', function (Request $request, Response $response, $args) {
  $id = $args['id'];
  $customer = Customer::find($id);
  $customer->delete();
  return $response->withStatus(200);
});
