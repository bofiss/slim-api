<?php
class Customer extends \Illuminate\Database\Eloquent\Model {
  public $timestamps = false;
  protected $table = "customers";
}
